# Potential Field #

This package contains a ROS node that uses a potential field to guide a robot and three 
follower robots. 

The potential field consists of two major forces: the attractive force and the repulsive force. 
 
The attractive force for the leader is centered at the goal. For the follower, it is centered on the 
leader. The attractive force for the followers was shaped slightly differently to allow them to be 
repulsed by the leader if they get too close. The attractive force has a cap of 10 put on it to keep it manageable and 
to achieve consistency across the stage with the repulsive force.  
 
The repulsive force is a cumulative force away from all objects read by the laser scan. The laser scan was processed 
by first converting the scan data into polar coordinates, then scaling or shaping the distance into 
a force, and then converting back to cartesian coordinates for averaging and adding to the attractive force. 
 
The method for detecting and declaring a leader is done through a publisher and subscriber on 
the topic “/whoIsLeader”.  
 
Final note: to turn on random walk mode, simply call the program without any goal parameters 
(eg. rosrun potential_field potential_field 0 1) 




![follow](images/follow.png)
![follow](images/follow2.png)