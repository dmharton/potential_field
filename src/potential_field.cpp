#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/Int32.h"
#include "sensor_msgs/LaserScan.h"
#include "tf/LinearMath/Quaternion.h" // Needed to convert rotation ...
#include "tf/LinearMath/Matrix3x3.h"  // ... quaternion into Euler angles


struct Pose {
    double x; // in simulated Stage units
    double y; // in simulated Stage units
    double heading; // in radians
    ros::Time t; // last received time

    // Construct a default pose object with the time set to 1970-01-01
    Pose() : x(0), y(0), heading(0), t(0.0) {};

    // Process incoming pose message for current robot
    void poseCallback(const nav_msgs::Odometry::ConstPtr &msg) {
        double roll, pitch;

        x = msg->pose.pose.position.x;
        y = msg->pose.pose.position.y;
        tf::Quaternion q = tf::Quaternion(
                msg->pose.pose.orientation.x,
                msg->pose.pose.orientation.y,
                msg->pose.pose.orientation.z,
                msg->pose.pose.orientation.w
        );
        tf::Matrix3x3(q).getRPY(roll, pitch, heading);
        t = msg->header.stamp;
    };
};

struct VectorP {
    double r, theta;
};

struct IdleKicker {
    ros::Duration idleCheckInterval;
    ros::Time idleCheckTime;
    Pose lastPose;
    int minDistanceThreshold;
    bool avoiding;
    double avoidanceDirection;
    ros::Duration avoidanceDuration;
};

class PotFieldBot {
public:
    // Construct a new Potential Field controller object and hook up
    // this ROS node to the simulated robot's pose, velocity control,
    // and laser topics
    PotFieldBot(ros::NodeHandle &nh, int robotID, int n, double gx, double gy, bool rw)
            : ID(robotID), numRobots(n), goalX(gx), goalY(gy), randomWalkEnabled(rw) {


        // Initialize random time generator
        srand((unsigned int) time(NULL));

        isLeader = false;
        leader = -1;

        if (numRobots > 1) {
            robotName = "/robot_" + boost::lexical_cast<std::string>(robotID);

            leaderPub = nh.advertise<std_msgs::Int32>("/whoIsLeader", 1);

            //wait a short bit to see if there is already a leader publishing
            ros::Rate poll_rate(100);
            ros::Time beginWait = ros::Time::now();

            while (leaderPub.getNumSubscribers() == 0 &&
                    beginWait + ros::Duration(2) < ros::Time::now()) {

                poll_rate.sleep();
            }

            leaderSub = nh.subscribe("/whoIsLeader", 1, &PotFieldBot::leaderCallback, this);

            if (leader == -1) {
                // if no leader, I'm the leader
                isLeader = true;
                leader = ID;
            }

        } else {
            robotName = "";
            isLeader = true; // I'm alone
            leader = ID;
        }

        // Subscribe to the current simulated robot's laser scan topic and
        // tell ROS to call this->laserCallback() whenever a new message
        // is published on that topic
        laserSub = nh.subscribe(robotName + "/base_scan", 1, &PotFieldBot::laserCallback, this);

        // Advertise a new publisher for the current simulated robot's
        // velocity command topic (the second argument indicates that
        // if multiple command messages are in the queue to be sent,
        // only the last command will be sent)
        commandPub = nh.advertise<geometry_msgs::Twist>(robotName + "/cmd_vel", 1);


        for (int i = 0; i < numRobots; i++) {
            pose.push_back(Pose());
        }


        for (int i = 0; i < numRobots; i++) {
            if (numRobots > 1) {
                robotName = "/robot_" + boost::lexical_cast<std::string>(i);
            }
            std::cout << robotName << std::endl;

            // Subscribe to each robot' ground truth pose topic
            // and tell ROS to call pose->poseCallback(...) whenever a new
            // message is published on that topic
            poseSubs.push_back(nh.subscribe(
                    robotName + "/base_pose_ground_truth", 1, &Pose::poseCallback, &pose[i]));

        }

        idleKicker.idleCheckInterval = ros::Duration(10);
        idleKicker.idleCheckTime = ros::Time::now() + idleKicker.idleCheckInterval;
        idleKicker.minDistanceThreshold = 3;
        idleKicker.avoidanceDuration = ros::Duration(15);
        idleKicker.avoiding = false;

        randomWalkTime = ros::Time::now();

    };

    // Send a velocity command
    void move(double linearVelMPS, double angularVelRadPS) {
        geometry_msgs::Twist msg; // The default constructor will set all commands to 0
        msg.linear.x = linearVelMPS;
        msg.angular.z = angularVelRadPS;
        commandPub.publish(msg);
    };


    // Process incoming laser scan message
    void laserCallback(const sensor_msgs::LaserScan::ConstPtr &msg) {

        // (see http://www.ros.org/doc/api/sensor_msgs/html/msg/LaserScan.html)
        unsigned int minIndex = (unsigned int) ceil((MIN_SCAN_ANGLE_RAD - msg->angle_min) / msg->angle_increment);
        unsigned int maxIndex = (unsigned int) ceil((MAX_SCAN_ANGLE_RAD - msg->angle_min) / msg->angle_increment);


        double totalX = 0, totalY = 0;
        int usableRanges = 0;
        for (int i = minIndex; i < maxIndex; i++) {
            float range = msg->ranges[i];

            VectorP repulsor = {
                    range, //r
                    pose[ID].heading + msg->angle_min + (msg->angle_increment * (i + 1))
            };

            if (range < msg->range_max && range > msg->range_min) {

                // find out if a follower is detected
                bool foundFollower = false;
                if (isLeader) {
                    for (int j = 0; j < pose.size(); j++) {
                        if (j != ID) {

                            double laserX = repulsor.r * cos(repulsor.theta) + pose[ID].x;
                            double laserY = repulsor.r * sin(repulsor.theta) + pose[ID].y;

                            double distFromFollower =
                                    sqrt(pow(laserX - pose[j].x, 2) + pow(laserY - pose[j].y, 2));

                            if (distFromFollower <= .75) {
                                foundFollower = true;

                            }
                        }
                    }// end pose loop
                }

                // make sure we aren't looking at a follower. Leader has the right of way.
                if (!foundFollower) {
                    usableRanges++;

                    //shape the force
                    repulsor.r = (20 / pow(repulsor.r - SAFE_DISTANCE, 2)) * REPULSE_FORCE;
                    if (range < SAFE_DISTANCE) {
                        repulsor.r = 999999;
                    }


                    totalX += repulsor.r * cos(repulsor.theta);
                    totalY += repulsor.r * sin(repulsor.theta);
                }
            }
        }//end ranges loop

        // average the number of data points to get the final value;
        repulseX = totalX / usableRanges;
        repulseY = totalY / usableRanges;


        //calculations for logging
        VectorP repulse = {
                sqrt(pow(repulseX, 2) + pow(repulseY, 2)),
                atan2(repulseY, repulseX) * 180 / M_PI
        };
        std::cout << "repulseRT = " << repulse.r << ", " << repulse.theta << std::endl;

    };


    void leaderCallback(const std_msgs::Int32::ConstPtr &msg) {

        if (ID != msg->data) {
            isLeader = false;
            leader = msg->data;
        }
    }

    // Main FSM loop for ensuring that ROS messages are
    // processed in a timely manner, and also for sending
    // velocity controls to the simulated robot based on the FSM state
    void spin() {
        ros::Rate rate(30); // Specify the FSM loop rate in Hz

        while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

            std::cout << "The Leader is " << leader << std::endl;

            //advertise that we're the leader
            if (isLeader) {
                if (numRobots > 1) {
                    std_msgs::Int32 msg;
                    msg.data = ID;
                    leaderPub.publish(msg);
                }

                // Check for randomWalk
                if (randomWalkEnabled && randomWalkTime <= ros::Time::now()) {
                    goalX = rand() % (MAP_WIDTH + 1) - MAP_WIDTH / 2;
                    goalY = rand() % (MAP_HEIGHT + 1) - MAP_HEIGHT / 2;

                    randomWalkTime = ros::Time::now() + RANDOM_WALK_INTERVAL;
                    idleKicker.idleCheckTime = ros::Time::now() + idleKicker.idleCheckInterval;
                    idleKicker.avoiding = false;
                }
                std::cout << "GoalXY: " << goalX << ", " << goalY << std::endl;
            }


            // calculate Attract force
            double attractX, attractY;
            if (isLeader) {
                attractX = goalX - pose[ID].x;
                attractY = goalY - pose[ID].y;
            } else {
                attractX = pose[leader].x - pose[ID].x;
                attractY = pose[leader].y - pose[ID].y;
            }

            // the "carrot on a stick" for mapping Swearingen
            /*VectorP attract = {
              10, //r
              pose[ID].heading //theta
            };
            */

            VectorP attract = {
                    sqrt(pow(attractX, 2) + pow(attractY, 2)), //r
                    atan2(attractY, attractX) //theta
            };

            //shape the attractive force to allow for stopping
            if (isLeader) {
                attract.r = std::min(pow(attract.r, 2), 10.0) * ATTRACT_FORCE;
            } else {
                // dips below zero to followers to get out of the leader's way more quickly
                attract.r = std::min(0.6 * pow(attract.r, 2) - 4, 10.0) * ATTRACT_FORCE;
            }
            if (idleKicker.avoiding) { //override attractive heading if in avoiding mode
                attract.theta = idleKicker.avoidanceDirection;
                std::cout << "**AVOIDING** ";// << std::endl;
            }

            std::cout << "attractRT = " << attract.r << ", " << attract.theta * 180 / M_PI << std::endl;

            attractX = attract.r * cos(attract.theta);
            attractY = attract.r * sin(attract.theta);



            // check if the robot is stuck at a local minima
            if (idleKicker.idleCheckTime < ros::Time::now()) {

                double distanceMoved = sqrt(
                        pow(pose[ID].x - idleKicker.lastPose.x, 2) +
                        pow(pose[ID].y - idleKicker.lastPose.y, 2)
                );

                //check if the robot has moved since we last checked and is not at the goal
                if (distanceMoved < idleKicker.minDistanceThreshold && attract.r >= 10) {

                    idleKicker.avoiding = true;
                    int scaledPI = (int) round(M_PI * 10000);
                    idleKicker.avoidanceDirection = (rand() % (scaledPI + 1) - scaledPI / 2) / 10000;
                    idleKicker.avoidanceDuration += ros::Duration(5);

                } else {
                    idleKicker.avoiding = false;
                }

                idleKicker.idleCheckTime = ros::Time::now();
                if (idleKicker.avoiding) {
                    idleKicker.idleCheckTime += idleKicker.avoidanceDuration;
                } else {
                    idleKicker.idleCheckTime += idleKicker.idleCheckInterval;
                }

                idleKicker.lastPose = pose[ID];
            }
            std::cout << "idleCheckTime: " << idleKicker.idleCheckTime - ros::Time::now() << std::endl;



            //// Sum all forces together to act on robot
            double forceX = attractX - repulseX;
            double forceY = attractY - repulseY;

            VectorP totalForce = {
                    sqrt(pow(forceX, 2) + pow(forceY, 2)), //r
                    atan2(forceY, forceX) //theta
            };

            //calculate movement direction
            double spin = totalForce.theta - pose[ID].heading;

            //allows for slowing down for very sharp turns
            double forwardScalar = std::max(std::min(-1 * fabs(spin / 2) + 1.3, 1.0), 0.0);

            // taking into account the 0,2pi discontinuity
            if (spin > M_PI || spin < -M_PI) {
                spin = M_PI - spin;
            }

            // calculate final foward speed
            double forward = std::min(totalForce.r, FORWARD_SPEED_MPS) * forwardScalar;

            // enforce max spin speed
            spin = std::max(std::min(spin, ROTATE_SPEED_RADPS), ROTATE_SPEED_RADPS * -1);

            //rounded values allow for stopping at the goal
            spin = round(spin * 100) / 100;
            forward = round(forward * 10) / 10;

            std::cout << "forward: " << forward << std::endl;
            std::cout << "spin: " << spin << std::endl;

            move(forward, spin);

            std::cout << "totalForceRT = " << totalForce.r << ", "
                      << totalForce.theta * 180 / M_PI << std::endl;


            // Demo code: print each robot's pose
            for (int i = 0; i < numRobots; i++) {
                std::cout << std::endl;
                std::cout << i << "        ";
                std::cout << "Pose: " << pose[i].x << ", " << pose[i].y << ", "
                          << pose[i].heading * 180 / M_PI << std::endl;
            }

            ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
            rate.sleep(); // Sleep for the rest of the cycle, to enforce the FSM loop rate
        }
    };

    // Tunable motion controller parameters
    const static double FORWARD_SPEED_MPS;
    const static double ROTATE_SPEED_RADPS;

    // potential field constants
    const static float ATTRACT_FORCE; // Attractive force towards goal
    const static float REPULSE_FORCE; // Repulsive force from obstacles
    const static float SAFE_DISTANCE; // minimum distance to keep away from obstacles

    const static int MAP_WIDTH, MAP_HEIGHT; // size of the map
    const static ros::Duration RANDOM_WALK_INTERVAL;


    const static double MIN_SCAN_ANGLE_RAD = -30.0 / 180 * M_PI;
    const static double MAX_SCAN_ANGLE_RAD = +30.0 / 180 * M_PI;

protected:
    ros::Publisher commandPub; // Publisher to the current robot's velocity command topic
    ros::Publisher leaderPub; // To let other robots know that it's here
    ros::Subscriber leaderSub; // To find out who is the leader
    ros::Subscriber laserSub; // Subscriber to the current robot's laser scan topic
    std::vector<ros::Subscriber> poseSubs; // List of subscribers to all robots' pose topics
    std::vector<Pose> pose; // List of pose objects for all robots
    int ID; // 0-indexed robot ID
    int numRobots; // Number of robots, positive value
    double goalX, goalY; // Coordinates of goal
    double repulseX, repulseY;
    IdleKicker idleKicker;
    ros::Time randomWalkTime;
    bool randomWalkEnabled;
    std::string robotName;
    bool isLeader; //designates the current robot as the leader
    int leader;
};

//constant definitions
const double PotFieldBot::FORWARD_SPEED_MPS = 2.0;
const double PotFieldBot::ROTATE_SPEED_RADPS = M_PI / 2;

const float PotFieldBot::ATTRACT_FORCE = 1.0;
const float PotFieldBot::REPULSE_FORCE = 1.0;
const float PotFieldBot::SAFE_DISTANCE = 0.5;

const int PotFieldBot::MAP_WIDTH = 50;
const int PotFieldBot::MAP_HEIGHT = 50;
const ros::Duration PotFieldBot::RANDOM_WALK_INTERVAL = ros::Duration(20);

int main(int argc, char **argv) {
    int robotID = -1, numRobots = 0;
    double goalX = 0, goalY = 0;
    bool printUsage = false;
    bool randomWalkEnabled = argc < 5;


    // Parse and validate input arguments
    if (argc < 3) {
        printUsage = true;
    } else {
        try {
            robotID = boost::lexical_cast<int>(argv[1]);
            numRobots = boost::lexical_cast<int>(argv[2]);

            if (!randomWalkEnabled) {
                goalX = boost::lexical_cast<double>(argv[3]);
                goalY = boost::lexical_cast<double>(argv[4]);
            }

            if (robotID < 0) { printUsage = true; }
            if (numRobots <= 0) { printUsage = true; }
        } catch (std::exception err) {
            printUsage = true;
        }
    }
    if (printUsage) {
        std::cout << "Usage: " << argv[0] << " [ROBOT_NUM_ID] [NUM_ROBOTS] ([GOAL_X] [GOAL_Y])" << std::endl;
        return EXIT_FAILURE;
    } else if (robotID + 1 > numRobots) {
        std::cout << "RobotID must be less than the total number of robots.";
        return EXIT_FAILURE;
    }


    ros::init(argc, argv, "potfieldbot_" + std::string(argv[1])); // Initiate ROS node
    ros::NodeHandle n;
    if (numRobots > 1) {
        n = ros::NodeHandle("/robot_" + std::string(argv[1])); // Create named handle "robot_#"
    }

    PotFieldBot robbie(n, robotID, numRobots, goalX, goalY, randomWalkEnabled); // Create new random walk object
    robbie.spin(); // Execute FSM loop

    return EXIT_SUCCESS;
};
